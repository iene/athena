/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#ifndef XAOD_ATHLYSIS

#include "TauClusterFinder.h"
#include "tauRecTools/HelperFunctions.h"

#include "xAODTau/TauJetContainer.h"
#include "xAODTau/TauJetAuxContainer.h"
#include "xAODTau/TauJet.h"
#include "xAODBase/IParticleHelpers.h"


TauClusterFinder::TauClusterFinder(const std::string& name) :
TauRecToolBase(name) {
}



StatusCode TauClusterFinder::execute(xAOD::TauJet& tau) const {
  if (! tau.jetLink().isValid()) {
    ATH_MSG_ERROR("Tau jet link is invalid.");
    return StatusCode::FAILURE;
  }
  const xAOD::Jet* jetSeed = tau.jet();
  
  // Find all the clusters in the JetConstituent
  std::vector<const xAOD::CaloCluster*> clusterList = getClusterList(*jetSeed);

  for (const xAOD::CaloCluster* cluster : clusterList) {
    // Clusters with negative energy will be thinned, and the elementlinks to these
    // clusters will not be valid. 
    if (cluster->e() <=0) continue;

    ElementLink<xAOD::IParticleContainer> linkToCluster;
    linkToCluster.toContainedElement(
      *(static_cast<const xAOD::IParticleContainer*> (cluster->container())), 
      static_cast<const xAOD::IParticle*>(cluster));
    tau.addClusterLink(linkToCluster);
  }

  return StatusCode::SUCCESS;
}



std::vector<const xAOD::CaloCluster*> TauClusterFinder::getClusterList(const xAOD::Jet& jet) const {
  std::vector<const xAOD::CaloCluster*> clusterList;

  xAOD::JetConstituentVector constituents = jet.getConstituents();
  for (const xAOD::JetConstituent* constituent : constituents) {
    ATH_MSG_DEBUG("JetConstituent: ");
    ATH_MSG_DEBUG("eta: " << constituent->eta() << " phi: " << constituent->phi() << " e: " << constituent->e()); 

    if( constituent->type() == xAOD::Type::CaloCluster ) {
	  const xAOD::CaloCluster* cluster = static_cast<const xAOD::CaloCluster*>(constituent->rawConstituent());
      ATH_MSG_DEBUG("CaloCluster: ");
	  ATH_MSG_DEBUG("eta: " << cluster->eta() << " phi: " << cluster->phi() << " e: " << cluster->e());
	  
      // Cluster in Topo jets is a shallow copy of CaloCalTopoCluster. Since jets may not be stored to AOD in R22,
      // we will not be able retrieve the cluster from the jets with AOD as input. To fix this, we retrieve
      // the cluster in the orignial container, i.e. CaloCalTopoCluster.
      const xAOD::IParticle* originalParticle = xAOD::getOriginalObject(*cluster);
      const xAOD::CaloCluster* orignialCluster = static_cast<const xAOD::CaloCluster*>(originalParticle);
	  clusterList.push_back(orignialCluster);
    }
    else if ( constituent->type() == xAOD::Type::ParticleFlow ) {
	  const xAOD::PFO* pfo = static_cast<const xAOD::PFO*>(constituent->rawConstituent());
	  
      if (pfo->isCharged()) continue;
	  if (pfo->nCaloCluster()!=1){
	    ATH_MSG_WARNING("Neutral PFO has " << std::to_string(pfo->nCaloCluster()) << " clusters, expected exactly 1!\n");
        continue;
      }
	  
      clusterList.push_back(pfo->cluster(0));
    }
    else {
	  ATH_MSG_WARNING("Seed jet constituent type not supported ! Skip");
	  continue;
    }
  }
  
  for (const xAOD::JetConstituent* constituent : constituents) {
    // There is only one type in the constituents
    if (constituent->type() != xAOD::Type::ParticleFlow) break;
	
    const xAOD::PFO* pfo = static_cast<const xAOD::PFO*>( constituent->rawConstituent() );
	if (! pfo->isCharged()) continue;

	for (u_int index=0; index<pfo->nCaloCluster(); index++){
	  const xAOD::CaloCluster* cluster = pfo->cluster(index);
	  // Check it is not duplicate of one in neutral list
	  if ( std::find(clusterList.begin(), clusterList.end(), cluster) == clusterList.end() ) {
	    clusterList.push_back(cluster);
	  }
	}
  }
 
  return clusterList;
} 

#endif
